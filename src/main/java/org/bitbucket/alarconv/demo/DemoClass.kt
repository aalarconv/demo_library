package org.bitbucket.alarconv.demo

/**
 * Created by Agustin Alarcon on 2/26/19.
 * Copyright (c) 2019 moofwd. All rights reserved.
 */

class DemoClass {
    fun test(): String {
        return "test"
    }


    fun sum(number: Int): Int {
        return number + 1
    }
}